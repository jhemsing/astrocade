
module block_ram #(
	parameter C_ADDR_WIDTH
) (
	input wire clk,

	input wire [C_ADDR_WIDTH:0] addr, 	// 16 bit address bus
	input wire [7:0]  data_in, 	// 8 bit data bus

	output wire [7:0] data_out,

	//input wire write_en, // write enable

);
	reg [7:0] memory [C_ADDR_WIDTH:0];

	// initialize the ROM
	initial begin
		memory[0] =  8'hDE;
		memory[1] =  8'hAD;
		memory[2] =  8'hBE;
		memory[3] =  8'hEF;
	end

	/*
	always @ (posedge clk) 
	begin
		if (write_en) 
		begin
			memory[addr] <= data_in;
		end
	end
	*/

	// this can be combinational, but timing is easier to meet with clocks
	always @ (posedge clk)
	begin
		data_out <= memory[addr];
	end
			
endmodule
