/*

RFSH# MC1 MC0
0 		0   0 	A[7:0] from Z80
0 		0   1 	A[7:0] from Z80
0 		1   0 	A[7:0] from Z80
0 		1   1 	A[7:0] from Z80
1 		0   0  	A[7:0] from Z80
1 		1   0 	D[7:0] from Z80
1 		1   1 	D[7:0] to Z80
 
Not documented in the table above is a special cycle which occurs in response to a Z80 Interrupt Acknowledge.
When the Z80 is acknowledging an interrupt, it simultaneously asserts its IORQ# and M1# ou tputs. This is detected by
the data chip, which responds by asserting MC[1:0] to 0x3, and outputting the contents of its Interrupt Feedback Register
(I/O address 0 x0D) o nto the Microcycle Bus. The control of MC[1:0] is done internal to the Data chip, but the additional
control provided by the Z80 RFSH# signal is implemented externally. The following schematic details the Z80-sid e
connections to the Microcycle Bus.

*/

module astrocade_addr_decode (
  input clk,

  // addres control signals
  input wire RFSH_N,
  input wire MC1,
  input wire MC2,

	output reg screen_ram_en,
	output reg other_ram_en,

  
  
  input wire [7:0] z80_data_in,
  input wire [7:0] z80_data_out,

  input wire [15:0] z80_addr,
  output reg [15:0] MXD
);

  wire [2:0] addr_ctrl;

  assign addr_ctrl = { RFSH_N, MC1, MC0 }; 
  
  always @ (*) begin
    case (addr_ctrl)
      3'b110:
        MXD[7:0] <= z80_data_in;
      3'b111:
        MXD[7:0] <= z80_data_out;
      default:
        MXD[7:0] <= z80_addr[7:0];
    endcase

    MXD[15:8] <= z80_addr[15:8;
  end

	always @ (*) begin
		if (z80_addr < 16'h4000) begin
			screen_ram_en <= 1'b1;
			other_ram_en  <= 1'b0;
		end else begin
			screen_ram_en <= 1'b0;
			other_ram_en  <= 1'b1;
		end
	end
			

endmodule
